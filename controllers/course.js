const Course = require("../model/Course");
const auth = require("../auth");

// Create a New Course
module.exports.addCourse = async (user, reqBody, res) => {
	if(user.isAdmin){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, err) =>{
			if(err){
				return false;
			}

			else{
				return true;
			}
		})
	}

	else{
		return(`You have no access.`)
	}
}

// Retrieving all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then (result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Updating a specific course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You have no access`);
	}
}

// Archiving a specific course
module.exports.archiveCourse = async (user, courseId, reqBody) => {
	if(user.isAdmin){
		let archivedCourse = {
			isActive : reqBody.isActive
		}

		return Course.findByIdAndUpdate(courseId, archivedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You have no access`);
	}
}

