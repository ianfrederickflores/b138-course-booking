const User = require("../model/Users");
const Course = require("../model/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exists
module.exports.checkEmailExist = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}

		else{
			return false;
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNum : reqBody.mobileNum,
		password : bcrypt.hashSync(reqBody.password, 10)		
	})

	return newUser.save().then((user, err) => {
		if(err){
			return false;
		}

		else{
			return true;
		}
	})
};

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}

		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}

			else{
				return false;
			}
			
		}
	})
};

// Retrieve User Details
module.exports.getUserDetail = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";

		return result;
	});
};

// Enroll User to a class
module.exports.enroll = async (user, data) => {
	if(user.isAdmin){
		// Add the courseId in the enrollments array of the user
		let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollment array
		user.enrollments.push({courseId: data.courseId});

		// Save the updated user information in the database
		return user.save().then((user, err) => {
			if(err){
				return false;
			}

			else{
				return true;
			}
		})
	})
		// Add userId to the course's enrollees array
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId:data.userId});
		// Save the updated course information in the database
		return course.save().then((course, err) => {
				if(err){
					return false;
				}

				else{
					return true;
				}
			})
		})
		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated){
			return true;
		}

		else{
			return false;
		}
	}

	else{
		return (`You have no access`);
	}		
}