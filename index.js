const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const app = express();
const courseRoutes = require("./routes/course");

// Connection to MongoDB
mongoose.connect("mongodb+srv://dbIanFlores:Nitefallbrigade2936@wdc028-course-booking.6gyek.mongodb.net/b138_to-do?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines the "/courses" string to be included for all course routes defined in the "course" route
app.use("/courses", courseRoutes);

// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});