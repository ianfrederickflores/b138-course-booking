const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if user's email already exist in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details 
// auth.verify method acts as a middleware to ensure that the user is logged in before they can access the specific app features
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getUserDetail({userId:userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	let data = {
		userId: req.body.userId,
		courseId :req.body.courseId
	}
	userController.enroll(user, data).then(resultFromController => res.send(resultFromController));
});

// Allows us to export the "router" object that will be accessed in "index.js"
module.exports = router;